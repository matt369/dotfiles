#!/bin/sh
#
# dp - turn display on and off
#
set -eu

usage() {
	[ -z "${1:-}" ] || echo "${0##*/}: $*" >&2
	echo "usage: ${0##*/} [-d display] { on | off }" >&2
	exit 1
}

main() {
	while getopts :d: opt; do
		case "$opt" in
			d) DISPLAY="$OPTARG" ;;
			:) usage "option requires an argument: $OPTARG" ;;
			*) usage "unrecognized option: $OPTARG" ;;
		esac
	done

	shift $((OPTIND - 1))
	[ "$#" -eq 1 ] || usage

	case "$1" in
		on)
			if [ -n "${DISPLAY:-}" ]; then
				xset dpms force off
				xset dpms force on
			else
				setterm --blank poke
			fi
		;;

		off)
			if [ -n "${DISPLAY:-}" ]; then
				xset dpms force off
			else
				setterm --blank force
			fi
		;;

		*)
			usage "unrecognized operand: $1"
		;;
	esac
}

main "$@"

# user directories
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"

# shell
export ENV="$XDG_CONFIG_HOME/sh/shrc"
export HISTFILE="$XDG_DATA_HOME/sh/history"
export HISTSIZE=100000
export PATH="$HOME/.local/bin:$PATH"
export PS1='\[\033[1;34m\]\w\[\033[0m\]\n\$ '
export VISUAL='vim'

# configuration files
export VIMINIT="source $XDG_CONFIG_HOME/vim/vimrc"
export XINITRC="$XDG_CONFIG_HOME/X11/xinitrc"

# high-DPI scaling
export GDK_SCALE=2
export GDK_DPI_SCALE='0.5'

# automatically start X on tty1
[ "$(tty)" = /dev/tty1 ] \
    && [ -z "${DISPLAY:-}" ] \
    && exec xinit -- vt1 >"$XDG_DATA_HOME/xorg/xinit.log" 2>&1
